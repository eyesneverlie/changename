import { ACTIONS } from '../constants/index';

const initialState = {
    users: [],
    loading: false,
    error: null,
    currentUser: null
};

export const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTIONS.REQUEST_BEGINS: {
            return {
                ...state,
                loading: true,
                error: null
            }
        }
        case ACTIONS.GET_USERS: {
            return {
                ...state,
                loading: false,
                users: action.payload
            }
        }
        case ACTIONS.GET_USER: {
            
            return {
                ...state,
                loading: false,
                currentUser: action.payload
            }
        }
        case ACTIONS.DELETE_USER: {
            const usersListFiltered = state.users.filter((user, index) => user.id !== action.payload);

            return {
                ...state,
                loading: false,
                users: usersListFiltered
            }
        }
        case ACTIONS.UPDATE_USER_NAME: {
            return {
                ...state,
                loading: false,
                users: state.users.map(user => user.id === action.currentUser.id
                    ? { ...user, name: action.payload }
                    : user
                ),
                currentUser: { ...action.currentUser, name: action.payload }
            }
        }
        case ACTIONS.REQUEST_ERROR: {
            return {
                ...state,
                loading: false,
                error: action.error
            }
        }
        default: {
            return state;
        }
    }
};

export default usersReducer;