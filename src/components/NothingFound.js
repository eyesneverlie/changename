import React from 'react';
import styled from 'styled-components';

export const NothingFound = () => {
    return (
        <StyledNothingFound>Nothing found</StyledNothingFound>
    );
};

const StyledNothingFound = styled.p`
    color: #fff;
    font-size: 24px;
    line-height: 32px;
    font-weight: bold;
    padding: 15px;
`;