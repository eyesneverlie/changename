import React from 'react';
import styled from 'styled-components';

export const Title = (props) => ({

    render () {
        return (
            <StyledTitle>{ props.title }</StyledTitle>
        );
    }
});

const StyledTitle = styled.h1`
    color: #fff;
    font-size: 24px;
    line-height: 32px;
    font-weight bold;
    margin: 0;
    padding: 15px;
`;