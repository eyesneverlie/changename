import React, { Component } from 'react';
import { UserRecord } from './UserRecord';
import { connect } from 'react-redux';
import { deleteUserById } from '../actions/index';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';

export class UserList extends Component {

    _renderUsersList = () => {
        const { users } = this.props;

        if (!users || users.length === 0) {
            return <NothingFound>No users found</NothingFound>;
        }
        return (
            <StyledList>
                {users.map((user, index) =>
                    <UserRecord key={ index }
                                user={ user }
                                onUserDelete={ this.props.onUserDelete }
                                onUserChange={ this.props.onUserChange }
                    />
                )}
            </StyledList>
        );
    };

    render() {
        return(
            this._renderUsersList()
        );
    }
}

const mapStateToProps = (state) => ({
    users: state.users
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        deleteUserById
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);

const StyledList = styled.ul`
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    list-style-type: none;
    margin: 0;
    padding: 0 15px;
`;

const NothingFound = styled.p`
    color: #fff;
    font-size: 24px;
    line-height: 32px;
    font-weight: bold;
    padding: 15px;
`;