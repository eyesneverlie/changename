import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export class UserRecord extends Component {

    render() {
        const { user, onUserDelete } = this.props;

        return (
            <StyledUserRecord>
                <React.Fragment>
                    { user.name}
                    <div className="buttons-wrapper">
                        <button className="button" type="button">
                            <Link to={`/user/${ user.id }`}>Edit</Link>
                        </button>
                        <button className="button" type="button" onClick={ () => onUserDelete( user.id ) }>Delete</button>
                    </div>
                </React.Fragment>
            </StyledUserRecord>
        )
    }
}

const StyledUserRecord = styled.li`
    color: #fff;
    border: 1px solid rgb(167, 165, 166);
    border-radius: 4px;
    display: flex;
    align-items: center;
    justify-content: space-between;    
    padding: 5px 15px;
    margin-bottom: 10px;
    box-sizing: border-box;
`;
