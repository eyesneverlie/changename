import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import {
    fetchUsers,
    fetchUserById,
    updateUserById
} from '../actions/index';
import PreloaderIcon from 'react-preloader-icon';
import Oval from 'react-preloader-icon/loaders/Oval';
import { ToastContainer } from 'react-toastify';
import styled from 'styled-components';

export class ChangeUserDetails extends Component {

    componentDidMount() {
        const { users, match } = this.props;

        this.props.fetchUserById(parseInt(match.params.id));

        if (users.length === 0) {
            this.props.fetchUsers();
        }
    }

    _handleNameChange = (newName) => {
        this.setState({
            currentName: newName
        });
    };

    _handleNameUpdate = (e, currentUser, newName) => {
        e.preventDefault();

        const { updateUserById } = this.props;

        if (newName.length > 0) {
            updateUserById(currentUser, newName);
        } else {
            toast.error('User name is required!', {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    };

    _renderContent = () => {
        const { loading, currentUser } = this.props;

        if (loading) {
            return <PreloaderIcon
                loader={Oval}
                size={54}
                strokeWidth={8} // min: 1, max: 50
                strokeColor="#F0AD4E"
                duration={800}
            />
        }

        if (!currentUser) {
            return <NothingFound>No user found</NothingFound>
        }

        return (
            <StyledWrapper>
                <form className="update-form">
                    <input type="text"
                           placeholder={ currentUser.name }
                           onChange={(e) => this._handleNameChange(e.target.value)}
                    />
                    <button className="button"
                            type="button"
                            onClick={ (e) => this._handleNameUpdate(e, currentUser, this.state.currentName) }
                    >
                        Save
                    </button>
                </form>
                <ToastContainer />
                <Link className="return-home" to="/">Return to home</Link>
            </StyledWrapper>
        );
    };

    render () {

        return (
            this._renderContent()
        );
    }
}

const mapStateToProps = (state) => ({
    users: state.users,
    loading: state.loading,
    error: state.error,
    currentUser: state.currentUser
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchUsers,
        fetchUserById,
        updateUserById
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangeUserDetails);

const StyledWrapper = styled.div`
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    max-width: 500px;
    margin: 50px auto;
    box-sizing: border-box;
`;

const NothingFound = styled.p`
    color: #fff;
    font-size: 24px;
    line-height: 32px;
    font-weight: bold;
    padding: 15px;
`;