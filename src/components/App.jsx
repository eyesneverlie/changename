import React, { Component } from 'react';
import { Title } from './Title';
import UserList from './UserList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    fetchUsers,
    fetchUserById,
    deleteUserById,
    updateUserById
} from '../actions/index';
import PreloaderIcon from 'react-preloader-icon';
import Oval from 'react-preloader-icon/loaders/Oval';
import { ToastContainer } from 'react-toastify';
import styled from 'styled-components';

export class App extends Component {

    componentDidMount() {
        const { users, fetchUsers } = this.props;

        if (users.length === 0) {
            fetchUsers();
        }
    }

    _handleUserDelete = (userId) => {
        const { deleteUserById } = this.props;

        deleteUserById(userId);
    };

    _renderContent = () => {

        const { loading } = this.props;

        return (loading)
        ? <PreloaderIcon
                loader={Oval}
                size={54}
                strokeWidth={8} // min: 1, max: 50
                strokeColor="#F0AD4E"
                duration={800}
            />
            : <React.Fragment>
                <StyledAppWrap>
                    <Title title="Users list:" />
                    <UserList onUserDelete={ this._handleUserDelete } />
                    <ToastContainer />
                </StyledAppWrap>
            </React.Fragment>;
    };


    render() {
        return(
           this._renderContent()
        );
    }
}

const mapStateToProps = (state) => ({
    users: state.users,
    loading: state.loading,
    error: state.error,
    currentUser: state.currentUser
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchUsers,
        fetchUserById,
        deleteUserById,
        updateUserById
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

const StyledAppWrap = styled.div`
    display: flex;
    margin: 0 auto;
    flex-direction: column;
    max-width: 500px;
    box-sizing: border-box;
`;