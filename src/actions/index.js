import { ACTIONS } from '../constants/index';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';
import axios from 'axios';

export const fetchUsers = () => dispatch => {
    dispatch({
        type: ACTIONS.REQUEST_BEGINS
    });

    axios.get('https://simple-rest-weld.herokuapp.com/users')
        .then(
            (response) => {
                dispatch({
                    type: ACTIONS.GET_USERS,
                    payload: response.data
                });
            }
        )
        .catch((error) => {
            dispatch({
                type: ACTIONS.REQUEST_ERROR,
                payload: [],
                error
            });
        });
};

export const fetchUserById = (id) => dispatch => {
    dispatch({
        type: ACTIONS.REQUEST_BEGINS
    });

    axios.get(`https://simple-rest-weld.herokuapp.com/users/${ id }`)
        .then(
            (response) => {
                dispatch({
                    type: ACTIONS.GET_USER,
                    payload: response.data
                });
            }
        )
        .catch((error) => {
            dispatch({
                type: ACTIONS.REQUEST_ERROR,
                payload: [],
                error
            });
        });
};

export const deleteUserById = (id) => dispatch => {
    dispatch({
        type: ACTIONS.REQUEST_BEGINS
    });

    axios.delete(`https://simple-rest-weld.herokuapp.com/user/${ id }`)
        .then(
            (response) => {
                dispatch({
                    type: ACTIONS.DELETE_USER,
                    payload: id
                });
                toast.success('User has been deleted', {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        )
        .catch((error) => {
            dispatch({
                type: ACTIONS.REQUEST_ERROR,
                payload: [],
                error
            });
        });
};

export const updateUserById = (currentUser, newName) => dispatch => {
    dispatch({
        type: ACTIONS.REQUEST_BEGINS
    });

    axios.put(`https://simple-rest-weld.herokuapp.com/user/${ currentUser.id }`)
        .then(
            (response) => {
                dispatch({
                    type: ACTIONS.UPDATE_USER_NAME,
                    payload: newName,
                    currentUser
                });
                toast.success('User has been updated', {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        )
        .catch((error) => {
            dispatch({
                type: ACTIONS.REQUEST_ERROR,
                payload: [],
                error
            });
        });
};